# Migración de trabajos de Apache Spark ML a Spark + Tensorflow en Kubeflow

---

[Summit_Kubeflow](https://databricks.com/session_eu19/migrating-apache-spark-ml-jobs-to-spark-tensorflow-on-kubeflow)

![KubeflowLogo](https://venturebeat.com/wp-content/uploads/2020/03/8a06547d-965d-4981-806a-6c11d559b893-1-e1583173705409.png?w=1200&strip=all)

>Resumen

Una mirada más cercana al tipo de código escrito por Data Scientists muestra que el código de Machine Learning es solo una pequeña parte del trabajo. La mayor parte del esfuerzo se dedica a la gestión de datos e infraestructura.

**¿Qué es Kubeflow?**

Kubeflow es un marco de orquestación nativo de Kubernetes para flujos de trabajo de Machine Learning. Desarrollado por Argo , permite orquestar tuberías complejas de varios pasos. Estrechamente integrado con Kubernetes, administra sus pods y ofrece una interfaz de usuario simple para comenzar y rastrear sus trabajos.

![kube](https://miro.medium.com/max/3200/0*TGNBehHc1-SNvv59)

Esta herramienta proporcionará un espacio de trabajo para componer, desplegar y gestionar flujos de trabajo de ML de extremo a extremo, lo que lo convierte en una solución híbrida que no se bloquea desde la creación de prototipos hasta la producción. También permite una experimentación rápida y fiable, por lo que los usuarios pueden probar muchas técnicas de ML para identificar qué es lo que funciona mejor para su aplicación.

![kubeflow](https://miro.medium.com/max/4662/1*6Dxn6LEMgZM9_oj3DBa3Wg.png)

